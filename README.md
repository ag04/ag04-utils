ag04-utils
==========

Collection of shared AG04 Java utilities across multiple projects.

## Requirements
* [Java JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
* [Maven](https://maven.apache.org/download.cgi)

## List of features

### ⚡️ Exceptions

- [ConcurrencyException](#ConcurrencyException)
- [EntityNotFoundException](#EntityNotFoundException)
- [ValidationFailedException](#ValidationFailedException)


### ⚪️  Interfaces
- [EntityMapper](#EntityMapper)
- [EntityMapperRegistry](#EntityMapperRegistry)

### 🔹 Components
- [DefaultEntityMapperRegistry](#DefaultEntityMapperRegistry)
- EnhancedDefaultKeyGenerator (documentation missing)


---
####  ConcurrencyException ⚡️


Exception to be thrown when attempting to modify a resource with stale data (i.e old version).
```Java
ConcurrencyException( String id, Long version, Long currentVersion[, Object entity] )
```
| Argument              | Type            | Description                       
| --------------------- |:----------------|:----------------------------------
| id                    | `String`        |  Unique exception identifier      
| version               | `Long`          |  Latest version of the resource   
| currentVersion        | `Long`          |  Provided resource version        
| entity                |`Object`         |  Latest resource object         

**entity** param is used to provide the latest resource object to the exception handler who can then return the latest resource object to the client who made the request with a stale copy of the resource.

##### 👉 Usage Example - Optimistick Locking

```Java
public Entity findByIdAndVersion( Long id, Long version ) {
	Entity entity = entityService.findById( id );
	if( version < entity.version )
		throw new ConcurrencyException( "fooAction", entity.version, version, entity  );
	return entity;
}
```
---
#### EntityNotFoundException ⚡️
Exception to be thrown when attempting to access/modify an entity that does not exist.

```Java
EntityNotFoundException( String id )
```
| Argument              | Type          | Description                  
| --------------------- |:--------------|:-----------------------------
| entityId              | `String`      |  Unique exception identifier 

##### 👉 Usage Example - Fetching an entity

```Java
public Entity findById( Long id ) {
	Entity entity = entityService.findById( id );
	if( entity == null )
		throw new EntityNotFoundException( id  );
	return entity;
}
```
---
#### ValidationFailedException ⚡️
Exception to be thrown when a validation yields errors.

```Java
ValidationFailedException(BindingResult errors)
```

| Argument                 | Type              | Description          
| ------------------------ | :---------------- | :--------------------
|  errors                  | `BindingResults`  |  errors object       

##### 👉 Usage Example

```Java
validator.validate( modelObject, bindingResult );
if ( bindingResult.hasErrors() )
    throw new ValidationFailedException( bindingResult );

```
---
#### EntityMapper < E, S > ⚪️
Generic mapper interface for conversion between domain objects (entities) and data transfer objects (dto).
```Java
S mapFromEntity( E entity, Locale locale[, Map<String, Object> config] )
E mapToEntity(S source, Locale locale[, Map<String, Object> config] )
```
---
#### EntityMapperRegistry  ⚪️
 [DefaultEntityMapperRegistry](#DefaultEntityMapperRegistry) is a default implementation of this interface.
```Java
EntityMapper getMapper(Object entity)
```

---
#### DefaultEntityMapperRegistry 🔹
 This is a default implementation of the [EntityMapperRegistry](#EntityMapperRegistry) which references all of the mappers implementing [EntityMapper](#EntityMapper).
##### Constructor
```Java
DefaultEntityMapperRegistry(Collection<EntityMapper> mappers)
```

| Argument             | Type                       | Description                  |
| ---------------------|:-------------------------- |:-----------------------------|
|  mappers             | `Collection<EntityMapper>` |  collection of mappers       |

#####.getMapper(Object entity)

Resolves a corresponding EntityMapper for given entity.
Invoking the `getMapper()` method with a parameter that is type of `Foo` yields a `FooMapper` if there is a `FooMapper` that implements the `EntityMapper`


##### 👉 Usage Example

```Java
Object entity = getEntityByUniqueId(); // Returns different types (ex. User, Event..)
EntityMapper mapper;
try {
	mapper = defaultMapperRegistry.getMapper(entity); // Resolve a mapper for entity
} catch(IllegalArgumentException ex) { 
	return null; // No mapper found for the entity 
}
Object entityDto = mapper.mapFromEntity(entity, null); 

```

##### 🔌 Java Configuration
```Java
@Configuration
public class SharedConfigurationReference {

    @Bean
    @Autowired
    public DefaultEntityMapperRegistry DefaultEntityMapperRegistry( 
		Collection<EntityMapper> mappers
	) {
        return new DefaultEntityMapperRegistry(mappers);
    }
}
```


### Setup (First time)
1. Clone the repository: `git clone https://YoutUserName@bitbucket.org/ag04/ag04-utils.git`
4. Build project with: ` mvn clean install `

### Release
1) mvn release:prepare

2) mvn release:perform


## Changelog

### Version 1.0.12

Upgraded:
* spring-boot to version 1.5.10.RELEASE 

### Version 1.0.11

Optimized DefaultEntityMapperRegistry to use String as key (insted of Class object) in a map of registered EventMappers,
thus avoiding the possibility of the memeory leak.


### Version 1.0.10

Upgraded:
* spring-boot to version 1.5.9.RELEASE 
* dbunit to 2.5.4

Added:

1. New property **Object entity** to the [ConcurrencyException](#ConcurrencyException) 

2. [EntityMapper](#EntityMapper) interface 

3. [EntityMapperRegistry](#EntityMapperRegistry) (interface) and [DefaultEntityMapperRegistry
(#DefaultEntityMapperRegistry) (implementation) 

### Version 1.0.9

Pravilno podesio ag04 nexus adresu

### Version 1.0.8 (NEUSPIO DEPLOY NA NEXUS)

Upgraded:

* spring-boot to version 1.4.5.RELEASE 
* dbunit to 2.5.2 

### Version 1.0.7

Everything is the same as with the 1.0.6 version, except that bug with maven-release plugin configuration has been fixed (plugin version was missing).

### Version 1.0.6

Upgraded:

* spring-boot to version 1.3.6.RELEASE 
* spring-test-dbunit to 1.3.0, 
* dbunit to 2.5.2 
* javaassist 3.20.0-GA

also cleaned up unnecessary lib/jar version definitions so that now all dependencies are resolved through spring.

#### Dependencies:

* spring-boot :: 1.3.6.RELEASE
    * spring-boot-starter
    * spring-boot-starter-test

* spring-test-dbunit 1.3.0

* joda-time :: 2.8.2 (resolved through spring-boot-parent)

* junit 4.12 (resolved through spring-boot-parent)

* dbunit 2.5.2

* javaassist 3.20.0-GA

* fest-assert 1.4

* mockito-core :: 1.10.19 (resolved through spring-boot-parent)

