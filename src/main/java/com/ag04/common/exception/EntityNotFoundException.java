package com.ag04.common.exception;

/**
 *
 * Created by dmadunic on 28/03/16.
 */
public class EntityNotFoundException extends RuntimeException {

    /**
     * Id of the entity that was not found.
     */
    private String entityId;

    public EntityNotFoundException(String entityId) {
        this.entityId = entityId;
    }

    public String getEntityId() {
        return entityId;
    }
}
