package com.ag04.common.exception;

import org.springframework.validation.BindingResult;

/**
 * Validation intended to be thrown by services, controllers or other components in case of validation errors.
 *
 * Created by dmadunic on 29/03/16.
 */
public class ValidationFailedException extends RuntimeException {

    private BindingResult errors;

    public ValidationFailedException(BindingResult errors) {
        this.errors = errors;
    }

    //--- set  / get methods --------------------------------------------------

    public BindingResult getErrors() {
        return errors;
    }

    public void setErrors(BindingResult errors) {
        this.errors = errors;
    }

}
