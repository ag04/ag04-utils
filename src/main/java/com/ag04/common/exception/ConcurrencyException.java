package com.ag04.common.exception;

/**
 *
 * Created by dmadunic on 28/03/16.
 */
public class ConcurrencyException extends RuntimeException {

    private String id;
    private Long version;
    private Long currentVersion;
    private Object entity;

    public ConcurrencyException(final String id, final Long version, final Long currentVersion){
        this(id, version, currentVersion, null);
    }

    public ConcurrencyException(final String id, final Long version, final Long currentVersion, Object entity){

        this.id = id;
        this.version= version;
        this.currentVersion = currentVersion;
        this.entity = entity;
    }

    //--- set  / get methods --------------------------------------------------

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public Long getVersion() { return version; }

    public void setVersion(Long version) { this.version = version; }

    public Long getCurrentVersion() { return currentVersion; }

    public void setCurrentVersion(Long currentVersion) { this.currentVersion = currentVersion; }

    public Object getEntity() { return entity; }

    public void setEntity(Object entity) { this.entity = entity; }
}
