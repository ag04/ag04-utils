package com.ag04.common.mapper;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Default implementation of EntityMapperRegistry. Relies on Map wit Class as a key and EntityMapper as value to resolve appropriate EntityMapper
 * for a specific Entity based on its class.
 *
 * @author: agulin
 */
public class DefaultEntityMapperRegistry implements EntityMapperRegistry {
    private Map<String, EntityMapper> mappedMappers = new HashMap<>();

    public DefaultEntityMapperRegistry(Collection<EntityMapper> mappers) {
        this.mappedMappers = mappers.stream().collect(Collectors.toMap(em -> em.getEntityClass().getCanonicalName(), Function.identity()));
    }

    @Override
    public EntityMapper getMapper(Object entity) {
        Class entityClass = entity.getClass();
        EntityMapper entityMapper = mappedMappers.get(entityClass.getCanonicalName());
        if(entityMapper == null) {
            throw new IllegalArgumentException("No mapper registered for entity (" + entityClass.getCanonicalName() + ")");
        }
        return entityMapper;
    }
}
