package com.ag04.common.mapper;

import com.ag04.common.mapper.EntityMapper;

/**
 * Interface defining the registry that holds multiple EntityMapper implementations.
 *
 * @author: agulin
 */
public interface EntityMapperRegistry {
    EntityMapper getMapper(Object entity);
}
