package com.ag04.common.mapper;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

/**
 *
 * Created by dmadunic on 14/12/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class DefaultEntityMapperRegistryTest {

    @Mock
    private EntityMapper entityMapperA;

    @Mock
    private EntityMapper entityMapperB;

    List<EntityMapper> mappers;

    @Before
    public void setup() {
        when(entityMapperA.getEntityClass()).thenReturn(String.class);
        when(entityMapperB.getEntityClass()).thenReturn(Double.class);

        mappers = new ArrayList<>();
        mappers.add(entityMapperA);
        mappers.add(entityMapperB);
    }

    @Test
    public void test_getMapper_whenMapperIsRegistered_thenCorrectMapperIsReturned() {
        // arrange ...
        DefaultEntityMapperRegistry registry = new DefaultEntityMapperRegistry(mappers);

        // act ...
        EntityMapper mapper = registry.getMapper(new String ("EntityOfTypeString"));

        // assert ...
        assertThat(mapper).isEqualTo(entityMapperA);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_getMapper_whenMapperIsunkonwn_thenExceptionIsThrown() {
        // arrange ...
        DefaultEntityMapperRegistry registry = new DefaultEntityMapperRegistry(mappers);
        DateTime entity = DateTime.now();
        // act ...
        EntityMapper mapper = registry.getMapper(entity);

    }

}
